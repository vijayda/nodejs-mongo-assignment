var express = require('express');
var router = express.Router();
const csvtojson = require("csvtojson");
const mongodb = require("mongodb").MongoClient;


let url = "mongodb://54.229.235.81:80/";

var connection = function (csvData) {
  mongodb.connect(
    url,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err, client) => {
      if (err) throw err;
      client
        .db("assignment")
        .collection("cities")
        .insertMany(csvData, (err, res) => {
          if (err) throw err;
          console.log(`Inserted: ${res.insertedCount} rows`);
          client.close();
        });
    }
  );
}
/* GET home page. */
router.get('/csvInsert', function (req, res, next) {
  res.render('index', { title: 'Node-MongoDB' });

  csvtojson()
    .fromFile("CityList.csv")
    .then(csvData => {
      console.log(csvData);
      connection(csvData);

    });

});

router.get('/state', function (req, res, next) {
  var search = req.query.q
  mongodb.connect(url, function (err, db) {
    if (err) throw err;
    var dbo = db.db("assignment");

    dbo.collection("cities").find({ State: new RegExp(search) }).toArray(function (err, docs) {

      console.log("Found the following states with the search string");

      var states = [];
      docs.forEach(function (doc) {

        states.push({
          "state": doc['State'],
          "district_code": doc['DistrictCode'],
          "district": doc['District']
        })
      })
      console.log(states)
      res.send(states);



      db.close();
    });
  });

});

router.get('/town', function (req, res, next) {
  var search = req.query.q
  mongodb.connect(url, function (err, db) {
    if (err) throw err;
    var dbo = db.db("assignment");

    dbo.collection("cities").find({ Town: new RegExp(search) }).toArray(function (err, docs) {

      console.log("Found the following town with the search string");

      var town = [];
      docs.forEach(function (doc) {

        town.push({
          "town": doc['Town'],
          "state": doc['State'],
          "district": doc['District']
        })
      })
      console.log(town)
      res.send(town);



      db.close();
    });
  });

});

router.get('/district', function (req, res, next) {
  var search = req.query.q
  mongodb.connect(url, function (err, db) {
    if (err) throw err;
    var dbo = db.db("assignment");

    dbo.collection("cities").find({ District: new RegExp(search) }).toArray(function (err, docs) {

      console.log("Found the following district with the search string");

      var district = [];
      docs.forEach(function (doc) {

        district.push({
          "town": doc['Town'],
          "urban_status": doc['UrbanStatus'],
          "state_code": doc['StateCode'],
          "state": doc['State'],
          "district_code": doc['DistrictCode'],
          "district": doc['District'],

        })
      })
      console.log(district)
      res.send(district);



      db.close();
    });
  });

});


module.exports = router;
